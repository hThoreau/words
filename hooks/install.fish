function install_"$package" -V path -V package

  # Check version
  if test (fish --version | string match -ar '\d' | string join '') -lt 300
    echo 'This plugin is compatible with fish version 3.0.0 or above, please update before retrying to install it' 1>&2
    return 1
  end

  # Install dependencies
  command wget -qO $path/dependency.fish \
  https://gitlab.com/lusiadas/dependency/raw/master/dependency.fish
  source $path/dependency.fish
  or return 1
  dependency -un $package curl grep ccrypt tor \
  -P https://gitlab.com/lusiadas/contains_opts
  if type -qf termux-info
    if not type -qf termux-clipboard-get
      wrn "|pgen| requires |termux-api| to manage clipboard content."
      reg "See installation instructions for it at https://wiki.termux.com/wiki/Termux:API"
    end
  end

  # Make a local copy from EFF's long wordlist
  test -e $path/default.cpt
  and return 0
  if not curl -s https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt --socks5 localhost:9050 | string match -ar '[^\d\s]+' >$path/default
    err "Unable to retrieve wordlist from EFF"
    reg "Update plugin with |omf update $package| to try again."
    return 1
  end

  # Encrypt it
  function "$package"_failed -V path
    rm $path/default.cpt
    err "Password lost. Encrypted wordlist was removed."
    reg "Update plugin with |omf update $package| to try again."
  end
  set -l password (echo (sort -R $path/default | head -6))
  command ccrypt -K $password $path/default
  win "Wordlist was downloaded and encrypted with a 6 word password"
  dim "Don't worry, you can change its password afterwards using the |-p/--password| option"

  # Output password
  reg -e "1. Display password\n2. Append to file\n3. Pipe to a command"
  read -n 1 -P "Chose an option [1-3]: " opt
  switch "$opt"
    case 1
      echo $password
    case 2
      read -P 'Path to file: ' opt
      if test -z "$opt"
        "$package"_failed
        return 1
      end
      set opt (string replace '~' $HOME $opt)
      mkdir -p (dirname $opt)
      echo $password >> $opt
    case 3
      read -P 'Command to execute: ' opt
      if not test "$opt"
        "$package"_failed
        return 1
      end
      if not echo $password | eval $opt
        "$package"_failed
        return 1
      end
    case '*' ''
      "$package"_failed
      return 1
  end
end

install_"$package"
set -l exit_status $status
functions -e dependency install_"$package"
test $exit_status -eq 0

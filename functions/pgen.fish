set -l cmd (command basename (status -f) | command cut -f 1 -d '.')
function $cmd -V cmd -d "Generate or increment password encrypted wordlists from which to retrieve passphrases"

  # Load dependencies
  source (command dirname (status -f))/../instructions.fish
  source (command dirname (status -f))/../dependency.fish
  set -l path (command dirname (status -f))/../
  function "$cmd"_main -V path -V cmd
    dependency -n $cmd ccrypt curl grep
    or return 1


    # Parse flags
    argparse -n $cmd -x (string join ' -x ' h,n {h,n},{"r,a,t,o",{c,w},p} w,{c,r} c,{a,t,o} | string split ' ') 'h/help' 'r/retrieve' 'a/add' 't/trim' 'o/overwrite' 'w/without-tor' 'c/clipboard' 'p/password=' 'n/new-password' -- $argv 2>"$PREFIX"/tmp/"$cmd"_err
    or return 1

    # Check if help option was invoked
    if set --query _flag_help
      pgen_instructions; string length -q $argv
      and return 1; or return 0
    end

    # Set variables
    set -l flag (set --name | string match -r -- '(?<=_flag_)[^wcp]$')
    set -l modifier (set --name | string match -r -- '(?<=_flag_)[wc]$')
    set -l quantity 6
    set -l wordlist
    set -l password

    # Parse arguments for option -n/--new-password
    if set --query _flag_new_password
      for arg in $argv
        test -e $arg
        or continue
        set wordlist (string replace '~' $HOME $arg)
        set --erase argv[(contains -i -- $arg $argv)]
        break
      end
      if contains -- - $argv
        if not isatty stdin
          while read -l line
            set password $password $line
          end
        else
          set password -
          set --erase argv[(contains -i -- $password $argv)]
        end
      else
        if test -n "$argv[1]"
          set password $argv[1]
          set --erase argv[1]
        end
      end

    # Parse arguments for other options
    else
      if string match -qr -- '^\d+$' $argv
        set quantity (string match -r -- '^\d+$' $argv)
        set --erase argv[(contains -i $quantity $argv)]
      end
      for arg in $argv
        test -e $arg
        or continue
        set wordlist (string replace '~' $HOME $arg)
        set --erase argv[(contains -i -- $arg $argv)]
        break
      end
      if set --query _flag_password
        if string match -q -- - $_flag_password
          if isatty stdin
            set password -
          else
            while read -l line
              set password $password $line
            end
          end
        else
          set password $_flag_password
        end
      end
    end

    # Set wordlist if an existing one hasn't been found among the arguments
    if test -z "$wordlist"
      if test -z "$argv"
        set wordlist (command realpath $path/default.cpt)
      else
        set wordlist $argv[1]
        set --erase argv[1]
      end
    end

    # Exit if unknown parameters are found.
    if test (count $argv) -eq 1
      err "$cmd: Unknown parameter |$argv|"
      return 1
    else if test (count $argv) -gt 1
      err "$cmd: Unknown parameters |"(string join "|, |" $argv)"|"
      return 1
    end

    # Test if wordlist is unencrypted
    set -l unencrypted
    if not test -e "$wordlist"
      set unencrypted true
    else if file -b "$wordlist" | string match -qv data
      wrn "File |$wordlist| is unencrypted."
      read -n 1 -P "Continue using this file? [y/n]: " | string match -qi y
      or return 1
      set unencrypted true

    # Ask for a password if it wasn't passed
    else if test -z "$password"
      read -sp 'wrn "Type password to decrypt wordlist: "' password
      or return 1
    end

    function "$cmd"_edit -V unencrypted -V cmd -a flag quantity wordlist \
    password modifier -d 'Edit a wordlist'

      # Decrypt wordlist
      if test -z "$unencrypted"
        command ccdecrypt --key "$password" $wordlist 2>"$PREFIX"/tmp/"$cmd"_err
        or return 1
      end
      set wordlist (string replace -r '\.cpt$' '' $wordlist)

      # Trim wordlist
      if string match -q -- t $flag
        head -n -$quantity $wordlist | cat > $wordlist

      # Othersise supplement or overwrite wordlist
      else

        # Check connection
        set -l dictionary https://www.wordnik.com/randoml
        set -l test_connection "command curl --connect-timeout 60 -o /dev/null -sIf $dictionary"
        dim -n "Testing connection... "
        if test -z "$modifier"
          dependency tor
          or return 1
          set test_connection $test_connection "--socks5 localhost:9050"
        end
        if not eval $test_connection
          err "$cmd: Failed to connect"
          return 1
        end

        # Fetch words
        set -l fetch_word command curl --connect-timeout 60 -s $dictionary
        test -z $modifier
        and set fetch_word $fetch_word "--socks5 localhost:9050"
        set -l wordcount 0
        string match -q $flag o
        and rm $wordlist 2>/dev/null
        mkdir -p (dirname $wordlist)
        while test $wordcount -ne $quantity
          set -l word (eval $fetch_word \
          | string match -r '(?<=/)\w+(?=/\?)')
          or continue
          string match -qar -- "^$word\$" (cat $wordlist 2>/dev/null)
          and continue
          echo $word >> $wordlist
          math $wordcount + 1 | read wordcount
          reg -en \r(tput el)"Fetched $wordcount/$quantity words"
        end
        echo

        # Encrypt wordlist
        if test -z "$unencrypted" -o -n "$password"
          command ccencrypt --key "$password" $wordlist
        else
          read -n 1 -p "wrn -n \"Would you like to encypt |$wordlist|? [y/n]: \"" \
          | string match -qi y
          and command ccencrypt $wordlist
        end
      end
    end

    function "$cmd"_retrieve -V unencrypted -V cmd -a quantity wordlist \
    password modifier -d 'Retrieve a passphrase'

      # Draw a passphrase from it
      set -l passphrase
      if test -n "$unencrypted"
        set passphrase (cat $wordlist 2>"$PREFIX"/tmp/"$cmd"_err \
        | string match -ar -- '^\w+' | shuf -n $quantity)
        or return 1
      else
        dim -n "Decrypting wordlist... "
        set passphrase (command ccat --key "$password" $wordlist 2>"$PREFIX"/tmp/"$cmd"_err \
        | string match -ar -- '^\w+' | shuf -n $quantity)
        or return 1
      end

      # Test if passphrase is valid
      if test -z "$passphrase"
        err -o "$cmd: Wordlist |$wordlist| is empty"
        return 1
      else if test (count $passphrase) -lt $quantity
        err -o "$cmd: Wordlist |$wordlist| has an insuficient amount of words to build the passphrase"
        return 1
      end

      # Return passphrase
      if test -z "$modifier"
        reg -o "$passphrase" 2>&1
      else
        if type -qf termux-info
          if not dpkg -s termux-api 2>/dev/null
            err -o "$cmd: |termux-api| is required to manage clipboard content."
            reg "See installation instructions for it at https://wiki.termux.com/wiki/Termux:API"
            return 1
          end
          echo -n $passphrase | command termux-clipboard-set
        else
          if not type -qf xclip
            wrn "|pgen| requires |xclip| to use manage clipboard content."
            dependency xclip
            or return 1
          end
          echo -n $passphrase | command xclip -sel clip
        end
        win -o "Passphrase copied to clipboard"
      end
    end

    # Select option
    switch "$flag"
      case a t o
        pgen_edit "$flag" "$quantity" "$wordlist" "$password" "$modifier"
      case n
        command ccrypt -K "$password" -x $wordlist 2>"$PREFIX"/tmp/"$cmd"_err
      case r ''
        pgen_retrieve "$quantity" "$wordlist" "$password" "$modifier"
    end
  end

  # Call main function, display error messages, and unload auxiliary functions
  "$cmd"_main $argv
  set -l exit_status $status
  test -s "$PREFIX"/tmp/"$cmd"_err
  and grep -oP '(?<=: ).+' "$PREFIX"/tmp/"$cmd"_err | err -o $cmd:
  rm "$PREFIX"/tmp/"$cmd"_err
  functions -e (functions | string match -ar "^$cmd.+") dependency
  test $exit_status -eq 0
end
